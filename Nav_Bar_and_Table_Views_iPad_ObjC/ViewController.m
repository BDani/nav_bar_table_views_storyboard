//
//  ViewController.m
//  Nav_Bar_and_Table_Views_iPad_ObjC
//
//  Created by Bruno Tavares on 05/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    //declaration of the table data array
    NSMutableArray *tableData;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //associate table view with delegate and dataSource
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //initialization of the table data array
    tableData = [[NSMutableArray alloc]initWithObjects:@"One", @"Go to View 2", @"Three", @"Four", @"Five", nil];
    
    self.tableView.rowHeight = 75;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableCell"];
    }
    
    cell.textLabel.text = tableData[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Row Selected: %@", tableData[indexPath.row]);
    
    if(indexPath.row == 1) {
        [self performSegueWithIdentifier:@"segueView2" sender:self];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

@end
