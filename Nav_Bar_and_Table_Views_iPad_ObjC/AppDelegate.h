//
//  AppDelegate.h
//  Nav_Bar_and_Table_Views_iPad_ObjC
//
//  Created by Bruno Tavares on 05/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

